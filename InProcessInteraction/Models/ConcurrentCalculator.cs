﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using InProcessInteraction.Interfaces;

namespace InProcessInteraction.Models
{
    public class ConcurrentCalculator : ICalculator
    {
        public int GetSum(List<int> numbers)
        {
            var groups = SplitIntoGroups(numbers, 10);
            var tasks = new Task<int>[10];
            var sw = new Stopwatch();
            sw.Start();

            for (int i = 0; i < groups.Count; i++)
            {
                var index = i;
                tasks[index] = Task.Run(() => groups[index].Sum());
            }

            Task.WaitAll(tasks.Cast<Task>().ToArray());
            sw.Stop();
            Console.WriteLine($"Concurrent sum duration: { (double)sw.ElapsedMilliseconds / 1000 } s.");

            return tasks.Select(x => x.Result).Sum();
        }

        private List<List<int>> SplitIntoGroups(List<int> numbers, int groupCount)
        {
            var result = new List<List<int>>();

            if (groupCount <= 1)
            {
                result.Add(numbers);
            }
            else
            {
                for (var i = 0; i < groupCount; i++)
                {
                    result.Add(i != groupCount - 1
                        ? numbers.Skip(i * groupCount).Take(groupCount).ToList()
                        : numbers.Skip(i * groupCount).ToList());
                }
            }

            return result;
        }
    }
}