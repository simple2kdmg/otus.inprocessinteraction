﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using InProcessInteraction.Interfaces;

namespace InProcessInteraction.Models
{
    public class SequentialCalculator : ICalculator
    {
        public int GetSum(List<int> numbers)
        {
            var sw = new Stopwatch();
            sw.Start();
            var result = numbers.Sum();
            sw.Stop();
            Console.WriteLine($"Sequential sum duration: { (double)sw.ElapsedMilliseconds / 1000 } s.");

            return result;
        }
    }
}