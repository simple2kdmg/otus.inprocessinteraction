﻿using System;
using System.Collections;
using System.Collections.Generic;
using InProcessInteraction.Interfaces;

namespace InProcessInteraction.Models
{
    public class NumberGenerator : INumberGenerator
    {
        public List<int> Generate(int count)
        {
            if (count < 1) throw new ArgumentOutOfRangeException(nameof(count), "Count should be more than 0.");
            var result = new List<int>();
            var random = new Random();

            for (int i = 0; i < count; i++)
            {
                result.Add(random.Next(1, 10));
            }

            return result;
        }
    }
}