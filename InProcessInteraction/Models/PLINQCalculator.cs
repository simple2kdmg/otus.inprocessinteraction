﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using InProcessInteraction.Interfaces;

namespace InProcessInteraction.Models
{
    public class PLINQCalculator : ICalculator
    {
        public int GetSum(List<int> numbers)
        {
            var sw = new Stopwatch();
            sw.Start();
            var result = numbers.AsParallel().Sum();
            sw.Stop();
            Console.WriteLine($"PLINQ Calculator sum duration: { (double)sw.ElapsedMilliseconds / 1000 } s.");
            return result;
        }
    }
}