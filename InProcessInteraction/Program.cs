﻿using System;
using InProcessInteraction.Controllers;
using InProcessInteraction.Models;

namespace InProcessInteraction
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = new ConsoleController().GetCount();
            var numbers = new NumberGenerator().Generate(count);

            new SequentialCalculator().GetSum(numbers);
            new ConcurrentCalculator().GetSum(numbers);
            new PLINQCalculator().GetSum(numbers);

            Console.ReadKey();
        }
    }
}
