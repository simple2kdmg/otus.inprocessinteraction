﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InProcessInteraction.Interfaces
{
    public interface ICalculator
    {
        int GetSum(List<int> numbers);
    }
}