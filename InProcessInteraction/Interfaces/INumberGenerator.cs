﻿using System.Collections;
using System.Collections.Generic;

namespace InProcessInteraction.Interfaces
{
    public interface INumberGenerator
    {
        List<int> Generate(int count);
    }
}