﻿using System;

namespace InProcessInteraction.Controllers
{
    public class ConsoleController
    {
        public int GetCount()
        {
            while (true)
            {
                Console.WriteLine("Enter numbers count (Count should be more than 0): ");
                var value = Console.ReadLine();
                Console.WriteLine(Environment.NewLine);
                if (int.TryParse(value, out var count))
                {
                    if (count < 1)
                    {
                        Console.WriteLine("Invalid count value. Count should be more than 0.");
                    }
                    else
                    {
                        return count;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid count value. Count should be more than 0.");
                }
            }
        }
    }
}